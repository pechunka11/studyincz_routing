const ExtractTextPlugin = require('extract-text-webpack-plugin');
var webpack = require('webpack');
var path = require('path');

module.exports = {
	context: __dirname,
	entry: {
		bundle: "./src/index.jsx",
		common: "./static/js/common.js",
		libs: "./static/js/libs.js",
		main_gmaps: "./static/js/main_gmaps.js"
	},
	output: {
		path: __dirname + '/public',
		filename: "[name].js",
		publicPath: '/public/'
	},
	module: {
		loaders: [
			{
				test: /\.js|.jsx?$/,
				exclude: /(node_modules)/,
				loader: 'babel-loader',
				query: {
					presets: ['react', 'es2015']
				}
			},
			{
				test: /\.(png|jpg)$/,
				loader: 'file-loader?name=[path][name].[ext]!extract-loader!html-loader'
			},
			{
				test: /\.sass$/,
				loader: ExtractTextPlugin.extract('css-loader!resolve-url-loader!sass-loader?sourceMap')
			}
		]
	},
	resolve: {
		extensions: ['.js', '.jsx'],
	},
	plugins: [
		new ExtractTextPlugin({ filename: 'app.css', allChunks: true }),
		new webpack.HotModuleReplacementPlugin()
	],
	devServer: {
		hot: true,
		historyApiFallback: true,
		contentBase: './'
	}
};