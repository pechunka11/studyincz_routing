import React from 'react';
import ReactDom from 'react-dom';

import Routes from './routes';

require('../static/sass/header.sass');
require('../static/sass/main.sass');


ReactDom.render(
  Routes,
  document.querySelector('#app')
);

if (module.hot) {
    module.hot.accept('./routes', () => {
        const newRoutes = require('./routes').default;
        ReactDom.render(
				  newRoutes,
				  document.querySelector('#app')
				);
    });
}
