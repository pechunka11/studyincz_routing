import React, { Component } from "react";

import Header from '../components/Header.jsx'
import Footer from '../components/Footer.jsx'
import Popup from '../components/Popup.jsx'



export default class App extends Component {
	render() {
		return (
			<div>
				<Header popup={this.handlePopup}/>
				{this.props.children}
				<Footer />
				<Popup {...this.state} changePopup={this.handlePopup}/>
			</div>
		);
	}
}