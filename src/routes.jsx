import React from 'react';
import { Route, IndexRoute, Router, browserHistory } from 'react-router';

import App from './views/layouts/app';
import Intro from './views/layouts/intro';
import Body from './views/layouts/body';
import TwoCols from './views/layouts/twoCols.jsx';
import ThreeCols from './views/layouts/threeCols.jsx';

import Home from './views/containers/home';
import middleSchool from './views/containers/education/middleSchool';
import AboutUs from './views/containers/about/about_us';
import Contacts from './views/containers/about/contacts';
import Represent from './views/containers/about/represent';
import Partners from './views/containers/about/partners';
import SlideItemsContainer from './views/containers/SlideItemsContainer';
import InsideProgram from './views/containers/education/InsideProgram';
import Catalog from './views/containers/Catalog'
import InsideLiving from './views/containers/InsideLiving'
import InsideUni from './views/containers/education/InsideUni'
import InsideBlog from './views/containers/InsideBlog'

import PersonalArea from './views/containers/user/PersonalArea'
import Payment from './views/containers/user/Payment'
import Quest from './views/containers/user/Quest'

import TableContainer from './views/containers/TableContainer'
import AddContainer from './views/containers/AddContainer'

export default(
	<Router history={browserHistory} >
	  <Route path='/' component={App}>

	  	<Route component={Intro}>
	    	<IndexRoute component={Home} />
	    </Route>

	    <Route component={Body}>
		    <Route component={TwoCols}>
		    	<Route path="education/middle-school" component={middleSchool} />
		    	<Route path="/catalog/:type/:city" component={Catalog} />
		    	<Route path="/catalog/:type" component={Catalog} />
		    	<Route path="about/contacts" component={Contacts} />

		    	<Route path="user_:user" component={PersonalArea} />

		    	<Route path="admin_:admin/table/:table" component={TableContainer} />
		    </Route>
		    <Route component={ThreeCols}>
	    		<Route path="education/more" component={SlideItemsContainer} />
	    		<Route path="education/:program" component={InsideProgram} />
	    		<Route path="education/university/:uni" component={InsideUni} />
	    		<Route path="about/company" component={AboutUs} />
	    		<Route path="about/represent" component={Represent} />
	    		<Route path="about/partners" component={Partners} />
	    		<Route path="more-info/:page" component={SlideItemsContainer} />
	    		<Route path="living/:accomodation" component={InsideLiving} />
	    		<Route path="media/blog/:item" component={InsideBlog} />

	    		<Route path="user_:user/payment" component={Payment} />
	    		<Route path="user_:user/quest/:quest" component={Quest} />

	    		<Route path="admin_:admin/add/:add" component={AddContainer} />
	    	</Route>
	    </Route>
	  
	  </Route>
	 </Router>
)
