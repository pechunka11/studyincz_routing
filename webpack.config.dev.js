var path = require('path');
var webpack = require('webpack');

module.exports = {
  context: __dirname,
  entry: {
    bundle: "./src/index.jsx",
    common: './static/js/common.js'
  },
  output: {
    path: path.resolve(__dirname, 'public/'),
    filename: "[name].js",
    publicPath: '/public/'
  },
  module: {
    loaders: [
      {
        test: /\.js|.jsx?$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015']
        }
      },
      { test: /\.(jpe?g|gif|png|svg|woff|ttf|wav|mp3)$/, loader: "file" },
      {
        test: /\.sass$/,
        loader: 'style-loader!css-loader!resolve-url-loader!sass-loader?sourceMap'
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  devServer: {
    historyApiFallback: true,
    contentBase: './'
  }
};